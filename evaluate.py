import os
import caffe
from PIL import Image
import numpy as np
import pickle
import random

model_definition = 'models/deploy_myNet.prototxt';
model_weights = 'models/FCN/l_iter_400.caffemodel';

net = caffe.Net(model_definition, model_weights, caffe.TEST);
transformer = caffe.io.Transformer({'data':net.blobs['data'].data.shape})
transformer.set_transpose('data',(2,0,1))
net.blobs['data'].reshape(1,3,384,384)
im = caffe.io.load_image('data/ADEChallengeData2016/images/validation/ADE_val_00000001.jpg')
net.blobs['data'].data[...] = transformer.preprocess('data',im)
out = net.forward()
pred = out['prob'].max(axis=1)
target = caffe.io.load_image('data/ADEChallengeData2016/annotations/validation/ADE_val_00000001.jpg')
