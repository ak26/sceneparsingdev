import numpy as np
import random
import GettingData
import tensorflow as tf

def ConvertToOneHotEncoding(label,NumOfClasses) :
	l = len(label)
	newLabel = np.zeros([l,NumOfClasses])
	for i in range(l) :
		newLabel[i][int(label[i])] = 1

	return newLabel

def weight_variable(shape):
	initial = tf.truncated_normal(shape, stddev=0.1)

	return tf.Variable(initial)

def bias_variable(shape):
	initial = tf.constant(0.01, shape=shape)
	return tf.Variable(initial)

def getBatchInputData(inp,imageFiles,batch_size,start,NumOfClasses) :
	first = start
	start = start + imageFiles
	length = len(inp)
	x1 = length
	#labels = np.zeros([batch_size,1])
	if start > x1 :
		first = 0
		perm = np.arange(x1)
		np.random.shuffle(perm)
		inp = inp[perm]
		start = imageFiles
	end = start
	#print inp[first]
	#print inp[end]
	#print 'getting data'
	data = GettingData.get_batch(inp[first],inp[end],int(batch_size/imageFiles))

	#labels[:,0] = label[first:end]
	label = ConvertToOneHotEncoding(data['y'],NumOfClasses)
	return data['x'] , label , start , inp

NumOfClasses = 151

trainingFiles = 20000
#training_iters = 100000
training_iters = 500
learning_rate = 0.02

lr_decay_rate = 0.75
lr_decay_step = 25000
CheckpointStep = 10000
displayStep = 10
InputFeatures = 64
imageFiles = 3
batch_size = imageFiles * 64
savingModel = 'Checkpoints/model1.ckpt'
start = 0
inpTemp = np.arange(trainingFiles)
inp,out,start,inpTemp = getBatchInputData(inpTemp,imageFiles,batch_size,start,NumOfClasses)
#print inp.shape
#print out.shape
#print inp[1]
#print inp[batch_size-1]


x = tf.placeholder(tf.float32, shape=[None, InputFeatures])
y_ = tf.placeholder(tf.float32, shape=[None, NumOfClasses])

W_fc1 = weight_variable([InputFeatures,100])
b_fc1 = bias_variable([100])

h1 = tf.nn.relu(tf.matmul(x,W_fc1)+b_fc1)


W_fc2 = weight_variable([100,200])
b_fc2 = bias_variable([200])


h2 = tf.nn.relu(tf.matmul(h1,W_fc2) + b_fc2)

keep_prob = tf.placeholder(tf.float32)
h2_drop = tf.nn.dropout(h2, keep_prob)



W_fc3 = weight_variable([200,100])
b_fc3 = bias_variable([100])


h3 = tf.nn.relu(tf.matmul(h2_drop,W_fc3) + b_fc3)

W_output = weight_variable([100,NumOfClasses])
b_output = bias_variable([NumOfClasses])

y = tf.nn.softmax(tf.matmul(h3,W_output) + b_output)


cross_entropy = -tf.reduce_sum(y_*tf.log(y))


global_step = tf.Variable(0, trainable=False)
lr = tf.train.exponential_decay(learning_rate,
								global_step,
								lr_decay_step,
								lr_decay_rate,
								staircase=True)
								
train_step = tf.train.AdamOptimizer(lr).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_,1), tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

sess = tf.Session()
saver = tf.train.Saver()

init = tf.initialize_all_variables()
sess.run(init)

start = 0
inpTemp = np.arange(trainingFiles)

for i in range(training_iters) :  

  inp,out,start,inpTemp = getBatchInputData(inpTemp,imageFiles,batch_size,start,NumOfClasses)

  
  
  
    
  if i % CheckpointStep == 0 or i == training_iters - 1:
    save_path = saver.save(sess, savingModel)
    print("Model saved in file: %s" % save_path)  
  

  _,accuracy1 = sess.run([train_step,accuracy],feed_dict={x: inp, y_: out, keep_prob : 0.5})
  if i%displayStep == 0 :
  	print accuracy1
  	print('after %d steps the training accuracy is %g'%(i,accuracy1))
    
print 'training done' 

