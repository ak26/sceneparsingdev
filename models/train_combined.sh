#!/usr/bin/env sh
set -e

/home/daivik/caffe/build/tools/caffe train --solver=/home/daivik/sceneparsing/models/solver_combined.prototxt --weights=combined.caffemodel  2> train.log
