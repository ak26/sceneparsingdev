import pickle as pkl
import numpy as np
from time import time
import sys
import tensorflow as tf
from tensorflow.python.ops import rnn, rnn_cell
from tensorflow.python.ops import ctc_ops as ctc
import save_img
def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def ConvertToOneHotEncoding(img_labels,NumOfClasses) :
	batch,dim1,dim2 = img_labels.shape
	newLabel = np.zeros([batch,dim1,dim2,NumOfClasses])
	for i in range(batch) :
		for j in range(dim1):
			for k in range(dim2):
				newLabel[i,j,k,int(img_labels[i,j,k])] = 1
	return newLabel

batch_size = None
dim1 = 384
dim2 = 384
nf = 64
nclass=151
hidden_size = 50
learning_rate = 0.02
lr_decay_rate = 0.75
lr_decay_step = 25000
CheckpointStep = 50
training_iters = 10000
displayStep = 3
p = 0.00007


l_in = tf.placeholder(tf.float32, shape=(batch_size, nf, dim1,dim2))
#n_batch, n_steps, in_size = l_in.get_shape()
targets = tf.placeholder(tf.float32,shape=(None,dim1,dim2,nclass))
targets_ = tf.reshape(targets,[-1,nclass])
#transpose to (dim1, batch,dim2, nf )
rnn_input_tr = tf.transpose(l_in,[2,0,3,1])
rnn_input_re = tf.reshape(rnn_input_tr,[-1,nf])
rnn_input = tf.split(0, dim1, rnn_input_re)
with tf.variable_scope('hori'):
	lstm_fw_cell = rnn_cell.BasicLSTMCell(hidden_size, forget_bias=1.0)
	lstm_bw_cell = rnn_cell.BasicLSTMCell(hidden_size, forget_bias=1.0)
	    # Get lstm cell output
	lstm_outputs, _, _ = rnn.bidirectional_rnn(lstm_fw_cell, lstm_bw_cell, rnn_input, dtype=tf.float32)
	lstm_outputs_re=tf.reshape(lstm_outputs, [dim1, -1, dim2, 2*hidden_size])
	lstm_output_tr=tf.transpose(lstm_outputs_re, [2,1,0,3])

rnn_input_re2 = tf.reshape(lstm_output_tr,[-1,nf])
rnn_input2 = tf.split(0, dim2, rnn_input_re2)
with tf.variable_scope('vert'):
	lstm_fw_cell2 = rnn_cell.BasicLSTMCell(hidden_size, forget_bias=1.0)
	lstm_bw_cell2 = rnn_cell.BasicLSTMCell(hidden_size, forget_bias=1.0)
	    # Get lstm cell output
	lstm_outputs2, _, _ = rnn.bidirectional_rnn(lstm_fw_cell2, lstm_bw_cell2, rnn_input2, dtype=tf.float32)
	lstm_outputs_re2=tf.reshape(lstm_outputs2, [dim2, -1, dim1, 2*hidden_size])
	lstm_output_tr2=tf.transpose(lstm_outputs_re, [1,2,0,3])



W2 = weight_variable([2*hidden_size,nclass])
b2 = bias_variable([nclass])
l_reshape3 = tf.reshape(lstm_output_tr2,[-1,2*hidden_size] )
Final_output = tf.nn.softmax(tf.matmul(l_reshape3,W2) + b2)

cross_entropy = -tf.reduce_sum(targets_*tf.log(Final_output))


global_step = tf.Variable(0, trainable=False)
lr = tf.train.exponential_decay(learning_rate,
								global_step,
								lr_decay_step,
								lr_decay_rate,
								staircase=True)
								
train_step = tf.train.AdamOptimizer(lr).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(targets_,1), tf.argmax(Final_output,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

sess = tf.Session()
saver = tf.train.Saver()

init = tf.initialize_all_variables()
sess.run(init)


for i in range(training_iters) :  
	datadict = save_img.get_batch(p)
	spx=datadict['x'].shape
	if(spx[0]==0):
		continue
	datadict['y'] = ConvertToOneHotEncoding(datadict['y'],nclass)
	print('batch_size = %d'%len(datadict['x']))
	if i %displayStep == 0 :	
		print('%d steps reached'%i)
		correct = sess.run([accuracy],feed_dict={l_in: datadict['x'], targets: datadict['y']})
		print('after %d steps the accuracy is %g'%(i,correct[0]))
	if i % CheckpointStep == 0 or i == training_iters - 1:
		save_path = saver.save(sess,  './model.ckpt',
                       global_step= 1+ i)
		print("Model saved in file: %s" % save_path)  
	sess.run([train_step],feed_dict={l_in: datadict['x'], targets: datadict['y']})
	del datadict

print 'training done' 

