import os
import caffe
from PIL import Image
import numpy as np
import pickle
import random
import math
## TO extract and save intermediate conv outputs
model_definition = '/home/abhishek/Desktop/MachineLearning/sceneparsingdev/models/truncmode.prototxt';
model_weights = '/home/abhishek/Desktop/MachineLearning/sceneparsingdev/FCN_iter_160000.caffemodel';
imageLocation = '/home/abhishek/Downloads/ADEChallengeData2016/images/training/ADE_train_'
annotationLocation = '/home/abhishek/Downloads/ADEChallengeData2016/annotations/training/ADE_train_'

net = caffe.Net(model_definition, model_weights, caffe.TEST);
def get_batch(start,end,batch_size):

	transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
	transformer.set_mean('data', np.load('/home/abhishek/deep-learning/caffe/python/caffe/imagenet/ilsvrc_2012_mean.npy').mean(1).mean(1))
	transformer.set_transpose('data', (2,0,1))
	transformer.set_channel_swap('data', (2,1,0)) 

	dat_x = []
	dat_y = []
	cnt = 0

	for h in range(start,end) :
		filename = str('00000000'+ str(h+1))
		filename = filename[len(filename)-8:]
		#print filename
		img = Image.open(imageLocation+filename+'.jpg')
		annotation = Image.open(annotationLocation+filename+'.png') 
		img = img.resize((384,384))
		net.blobs['data'].data[...] = transformer.preprocess('data', np.array(img))
		out = net.forward()
		info = out['conv1_2']
		info = info[0,:,100:484,100:484]
		annotation = np.array(annotation.resize( (384,384) ) )
		temp1 = int(math.sqrt(batch_size))
		#print temp1
		arx = random.sample(range(0,384),temp1)
		ary = random.sample(range(0,384),temp1)
		#print arx
		#print ary
		for i in range(temp1) :
			for j in range(temp1) :
				dat_x.append(info[:,arx[i],ary[j]])
				dat_y.append(annotation[arx[i],ary[j]])
		#dat_x.append(info)
		#dat_y.append(annotation)

	'''	
	for f in os.listdir('/home/daivik/sceneparsing/ADEChallengeData2016/images/training'):
		prob= random.random()
		if(prob>prob1):#sandomly choose 1% of images. Will take 5hrs otherwise
			continue
		img = Image.open('/home/daivik/sceneparsing/ADEChallengeData2016/images/training/'+f)
		img = img.resize((384,384))
		net.blobs['data'].data[...] = transformer.preprocess('data', np.array(img))
		out = net.forward()
		info = out['conv1_2']
		info = info[0,:,100:484,100:484]#correct for convolutions
		annotation = Image.open('/home/daivik/sceneparsing/ADEChallengeData2016/annotations/training/'+f.replace('.jpg','.png'))
		annotation = np.array(annotation.resize( (384,384) ) )
		dat_x.append(info)
		dat_y.append(annotation)
		cnt = cnt + 1
		print(cnt)
	'''	
	dick = {'x':np.array(dat_x), 'y':np.array(dat_y)}
	return dick


